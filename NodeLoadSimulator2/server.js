﻿var app = require('http').createServer(handler),
    io = require('socket.io-client'), //.listen(app),
    path = require('path'),
    fs = require('fs'),
    und = require('underscore'),
    moment = require('moment'),
    messageBuilder = require('code/messageBuilder').init();

var urls = ['http://localhost:8887/analyticshub?myauthtoken=mySecret', 'http://localhost:9998/analyticshub?myauthtoken=mySecret'], sockets = [];

app.listen(process.env.PORT || 8877);

var clients = [],
    timeouts = [],
    newUserInterval = 5000;

for (var i = 0; i < 10; i++) {
    var createTime = (i + 1) * newUserInterval;
    console.log('user will be created in about ' + createTime  + 'ms');
    console.log('current time: ' + moment().format());
  
    setTimeout(function () {
        var sendInterval = Math.floor((Math.random() * 30000) + 10000);
        var disconnectTime = sendInterval * Math.floor((Math.random() * 100) + 1);
       
        clients[i] = new SimulatedClient(urls[1], sendInterval, disconnectTime);
    }, createTime);
};


function handler(req, res) {
    res.writeHead(200, {'Content-Type': 'text/plain'});
    res.end('Hello World\n');
}

function SimulatedClient (url,  interval, disconnectTime) {
    var that = this, clicks = 0, isConnected;

    var client = io.connect(url, { transports: ['websocket'], 'force new connection': true });

    client.on('message', function (data) {
        processReceivedData(data);
    });

    client.on('connect', function () {
        isConnected = true;
        console.log('Now connected to ' + url + ', connection ID=' + client.socket.sessionid + ' UniqueId=');
        console.log('send interval: ' + interval + ', disconnect after: ' + moment().add('milliseconds', disconnectTime).format());
        console.log('current time: ' + moment().format());

        startSending();

        setTimeout(function() {
            disconnect();
        }, disconnectTime);
    });

    client.on('connect_failed', function (reason) {
        console.log('Could not connect to ' + url + ": " + reason);
    });

    
    function disconnect() {
        console.log('Disconnected from ' + url + ', connection ID=' + client.socket.sessionid + ' UniqueId=');
        isConnected = false;
        client.disconnect();
        client = null;
    }
        
    function send() {
        if (!isConnected) {
            return;
        }
        client.emit('send', messageBuilder.buildClickMessage());
        client.emit('send', messageBuilder.buildServiceCallMessage());
        client.emit('send', messageBuilder.buildPerformanceMessage());
        console.log('Invocation of send from ' + client.socket.sessionid + ' to ' + url + ' succeeded at ' + moment().format());
    }
        
    function processReceivedData(data) {
        if (data.type === 'greeting') {
            console.log('unique id: ' + data.message);
        }
    }
    
    //function buildMessage() {
    //    clicks++;
    //    return ('Time: ' + new Date()).valueOf() + ' Clicks: ' + clicks;
    //}
    
    function startSending() {
        setInterval(function() {
            send();
        }, interval);

    }

    return {
        client: client
    };
};

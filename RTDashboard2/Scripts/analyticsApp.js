﻿define(['signalR', 'socketio', 'moment', 'rx', 'rxBinding', 'performanceGraph', 'histogram', 'bubblesSvg'],
    function (signalR, socketio, moment, rx, rxBinding, PerformanceGraph, Histogram, BubblesSvg) {
    console.log("SignalR version: ", $.signalR.version);
    console.log("SocketIO version: ", io.version);
    
    //https://gist.github.com/mattpodwysocki/2834028
    //https://github.com/Reactive-Extensions/RxJS/blob/master/doc/howdoi/wrap.md
    //https://github.com/markmarkoh/datamaps/blob/master/README.md#getting-started
    var analyticsDashboardViewModel = (function() {
        var that = this,
            d3Map,
            performanceData = ko.observableArray([]),
            mapper = { 
                init: 'rgb(0,191,255)', //deep sky blue
                func: 'rgb(70,130,180)', //steel blue
                func2: 'rgb(123,104,238)',//medium slate blue
                func3: 'rgb(216,191,216)', //thistle
                func12: 'rgb(176,196,222)', //light steel blue
                func19: 'rgb(153,50,204)' //dark orchid
            },
            serviceCallsExecutionTimes1 = [],
            serviceCallsExecutionTimes2 = [],
            serviceCallsExecutionTimes3 = [],
            serviceCallsExecutionTimes4 = [],
            serviceCallsExecutionTimes5 = [],
            bubblesData = [],
            bubbleColors = ['#dda0dd', '#ffa500', '#eedd82', '#9acd32', '#48d1cc'];
        this.trackingId = '';
        this.serverInfo = ko.observable('None Selected');
        this.realtimeServer = ko.observable('');
        this.clicksCount = ko.observable(0);
        this.serviceCalls = ko.observableArray([]);

        that.summary = {};
        that.summary.activeUsers = ko.observable();
        that.summary.inactiveUsers = ko.observable();
        that.summary.firstUserConnectedAt = ko.observable();
        that.summary.lastUserConnectedAt = ko.observable();
        that.summary.lastUserActivityAt = ko.observable();

        that.activeUsersPercent = ko.computed(function () {
            return computeWidth(that.summary.activeUsers());
        });
        
        that.inactiveUsersPercent = ko.computed(function () {
            return computeWidth(that.summary.inactiveUsers());
        });

        that.activeUsersX = ko.computed(function() {
            return 150 - computeWidth(that.summary.activeUsers());
        });
        
        function computeWidth(users) {
            var all = that.summary.activeUsers() + that.summary.inactiveUsers();
            if (all > 0) {
                return 100 * users / all;
            } else {
                return 0;
            }
        }

        this.logger = function() {
            this.log = function(message) {
                console.log(message);
            };
            return this;
        }();
        this.source = null;
        
        this.init = function (realtimeSrvr, map) {
            if (that.disconnetHandler) {
                that.disconnetHandler();
            }
            if (!realtimeSrvr || !that[realtimeSrvr + 'InitHandler']) {
                return;
            }
            that.serverInfo(realtimeSrvr);
            
            that.source = that[realtimeSrvr + 'InitHandler'](that);

            d3Map = map;

            var t = (new Date()).valueOf(), // 1297110663, // start time (seconds since epoch)
                v = 0,
                emptyRange = d3.range(60).map(next);

            function next() {
                return {
                    time: ++t,
                    value: v = 0
                };
            }

            performanceData(emptyRange);

            var performanceGraph1 = new PerformanceGraph('#performanceGraph1', performanceData(), mapper);

            var histogram1 = new Histogram('#histogram1', []);
            var histogram2 = new Histogram('#histogram2', []);
            var histogram3 = new Histogram('#histogram3', []);
            var histogram4 = new Histogram('#histogram4', []);
            var histogram5 = new Histogram('#histogram5', []);

            //var bubbles = new BubblesSvg('#bubblesDiv', []);
            var bubbles = new BubbleChart({
                canvasId: "bubblesCanvas",
                metric: "",
                usedArea: 0.95,
                contain: false,
                textColor: "#fff"
                //popoverOpts: {
                //    textFont: "Open Sans",
                //},
                //data: []
            });
            //bubbles.paint();
            
            function createObserver(action) {
                return Rx.Observer.create(
                    function (data) {
                        //console.log('Next: ' + tag + x);
                        console.log("............................................");
                        console.log(JSON.stringify(data));
                        action(data);
                    },
                    function (err) {
                        console.log('Error: ' + err);   
                    },
                    function () {
                        console.log('Completed');   
                    });
            }

            var published = source.publish(); //.refCount();

            published.filter(function (data) {
                    return data && data.type === 'summary';
                }).subscribe(createObserver(processSummaryData));
            
            published.filter(function(data) {
                    return data && data.type === 'clickCount';
            }).subscribe(createObserver(processClickCount));
            
            published.filter(function (data) {
                return data && data.type === 'serviceCall';
            }).subscribe(createObserver(processServiceCall));
            
            published.filter(function (data) {
                return data && data.type === 'performance';
            }).subscribe(createObserver(processPerformanceCall));

            published.connect();
            
            function processSummaryData(data) {
                var summary = data.message;
                that.summary.activeUsers(summary.activeUsers);
                that.summary.inactiveUsers(summary.inactiveUsers);
                that.summary.firstUserConnectedAt(formatDateTime(summary.firstUserConnectedAt));
                that.summary.lastUserConnectedAt(formatDateTime(summary.lastUserConnectedAt));
            }
            
            function processClickCount(data) {
                that.logger.log('clicks count: ' + that.clicksCount());
                that.clicksCount(that.clicksCount() + 1);
                that.summary.lastUserActivityAt(formatDateTime(data.message.timestamp));
            }
            
            function processServiceCall(data) {
                var msg = data.message.message, bb = msg.rj.bb, item;
                item = {
                    name: msg.su,
                    radius: 25,
                    fillKey: msg.su.substring(msg.su.lastIndexOf('/') + 1),
                    latitude: 0.5 * (bb.tl[0] + bb.br[0]),
                    longitude: 0.5 * (bb.tl[1] + bb.br[1]),
                    results: msg.rc
                };
                that.serviceCalls.push(item);
                that.summary.lastUserActivityAt(formatDateTime(data.message.timestamp));

                var updated = updateServiceCallsExecutionTimes(+msg.su.substring(msg.su.length - 1), Math.round(msg.et * 0.01));

                updateBubblesData(msg.su, msg.rj);
               
                d3Map.bubbles(that.serviceCalls(), {
                    popupTemplate: function (geo, dt) {
                        return ['<div class="hoverinfo">' + dt.name,
                        '<br/>Results Count: ' + dt.results,
                        '<br/>Location: ' + dt.latitude + ', ' + dt.longitude,
                        '</div>'].join('');
                    },
                    fillOpacity: 0.25,
                    highlightOnHover: true,
                    highlightFillColor: '#FFFF00',
                    highlightFillOpacity: 0.85
                });

                updated.h.refresh(_.map(_.countBy(updated.s, function(x) {
                    return x;
                }), function(num, key) {
                    return { x: key, y: num };
                }));

                //bubbles.refresh({ name: 'root', children: bubblesData });
                //chart.spinner.start();
                //chart.data = myCustomData();
                //chart.reload()
                //chart.spinner.stop();
                //chart.paint();
                bubbles.data = bubblesData;
                bubbles.reload();
                bubbles.paint();
            }
            
            function updateBubblesData(su, m) {
                //var params = [];
                var key1, key2, match, subMatch;
                ['param2', 'param3', 'param4', 'param5', 'param6', 'param7', 'param8', 'param9', 'param10'].forEach(function (item, i) {
                    if (m[item]) {
                        key1 = getBubbleColor(+su.substring(su.length - 1));
                        key2 = item;
                        //params.push({ group: m.su.substring(m.su.length - 1), value: item });
                        //packageName: name, className: node.name, value: node.size
                        //match = _.findWhere(bubblesData, { packageName: key1, className: key2 });
                        //if (match) {
                        //    match.value = value + 1;
                        //} else {
                        //    bubblesData.push({ packageName: key1, className: key2, value: 1 });
                        //}
                        
                        //match = _.findWhere(bubblesData, { name: key1 });
                        //if (match) {
                        //    subMatch = _.findWhere(match.children, { name: key2 });
                        //    if (subMatch) {
                        //        subMatch.size = subMatch.size + 1;
                        //    } else {
                        //        subMatch = { name: key2, size: 1 };
                        //        match.children.push(subMatch);
                        //    }
                        //} else {
                        //    match = { name: key1, children: [{ name: key2, size: 1 }] };
                        //    bubblesData.push(match);
                        //}
                        
                        match = _.findWhere(bubblesData, { fillColor: key1, label: key2 });
                        if (match) {
                            match.data = match.data + 1;
                        } else {
                            bubblesData.push({ fillColor: key1, label: key2, data: 1 });
                        }
                        
                        //{
                        //    label: "Name of the thing",
                        //    data: 10,
                        //    metric: "this specific metric",
                        //    fillColor: '#234',
                        //    borderColor: "#FFF",
                        //    borderSize: 3,
                        //    }
                    }
                });
            }
            
            function getBubbleColor(val) {
                return bubbleColors[val - 1];
            }
            
            function updateServiceCallsExecutionTimes(id, et) {
                switch (id) {
                    case 1:
                        serviceCallsExecutionTimes1.push(et);
                        return { s: serviceCallsExecutionTimes1, h: histogram1 };
                    case 2:
                        serviceCallsExecutionTimes2.push(et);
                        return { s: serviceCallsExecutionTimes2, h: histogram2 };
                    case 3:
                        serviceCallsExecutionTimes3.push(et);
                        return { s: serviceCallsExecutionTimes3, h: histogram3 };
                    case 4:
                        serviceCallsExecutionTimes4.push(et);
                        return { s: serviceCallsExecutionTimes4, h: histogram4 };
                    case 5:
                        serviceCallsExecutionTimes5.push(et);
                        return { s: serviceCallsExecutionTimes5, h: histogram5 };
                }
            }
            
            function processPerformanceCall(data) {
                var msg = data.message.message;
                performanceData.shift();
                performanceData.push({ id: msg.id, value: msg.et, time: msg.tm });
                performanceGraph1.refresh(performanceData());
            }
        };

        this.signalRInitHandler = signalRInit;
        this.nodejsInitHandler = nodejsInit;
        this.iisnodeInitHandler = iisnodeInit;
        this.disconnetHandler = null;
        

        function signalRInit() {
            return Rx.Observable.create(function(observer) {
                var url = 'http://localhost/RealTimeSignalRServer2/';
                var connection = $.hubConnection(url);
                connection.logging = true;
                var proxy = connection.createHubProxy('dashboardhub');
                proxy.on('message', function(data) {
                    observer.onNext(data);
                });
                connection.start()
                    .done(function() {
                        that.logger.log('Now connected to ' + url + ', connection ID=' + connection.id + ' UniqueId=');
                    })
                    .fail(function () {
                        observer.onError(url);
                         that.logger.log('Could not connect to ' + url);
                    });

                that.disconnetHandler = function() {
                    that.logger.log('Disconnected from ' + url);
                };
            });
        }

        function nodejsInit() {
            var url = 'http://localhost:8887/dashboardhub?myauthtoken=mySecretDashboard';
            return nodejsCommon(url);
        }

        function iisnodeInit() {
            var url = 'http://localhost:9998/dashboardhub?myauthtoken=mySecretDashboard';
            return nodejsCommon(url);
        }

        function nodejsCommon(url) {
            return Rx.Observable.create(function(observer) {
                var client = io.connect(url);

                client.on('message', function(data) {
                    observer.onNext(data);
                });

                client.on('connect', function() {
                    that.logger.log('Now connected to ' + url + ', connection ID=' + client.socket.sessionid + ' UniqueId=');
                });

                client.on('connect_failed', function (reason) {
                    that.logger.log('Could not connect to ' + url + ": " + reason);
                    observer.onError(url);
                });

                that.disconnetHandler = function() {
                    that.logger.log('Disconnected from ' + url);
                    client.disconnect();
                };
            });
        }
            
        function formatDateTime(dateTiemValue) {
            if (dateTiemValue) {
                return moment(dateTiemValue).format('YYYY/MM/DD HH:mm:ss.SSS');
            }
            return '';
        }
        
        return this;

    })();

    return analyticsDashboardViewModel;
});
﻿define(['signalR', 'socketio', 'moment'], function (signalR, socketio, moment) {
    console.log("SignalR version: ", $.signalR.version);
    console.log("SocketIO version: ", io.version);
    
    var dashboardViewModel = (function() {
        var that = this;
        this.trackingId = '';
        this.serverInfo = ko.observable('None Selected');
        this.realtimeServer = ko.observable('');
        this.dataItems = ko.observableArray();
        this.users = ko.observableArray();
        this.summary = {};
        this.summary.activeUsers = ko.observable();
        this.summary.inactiveUsers = ko.observable();
        this.summary.firstUserConnectedAt = ko.observable();
        this.summary.lastUserConnectedAt = ko.observable();
        this.summary.lastUserActivityAt = ko.observable();
        this.summary.longestActivity = ko.observable();
        this.summary.shortestActivity = ko.observable();
        this.summary.medianActivity = ko.observable();
        this.summary.averageActivity = ko.observable();
        this.logger = function() {
            this.log = function(message) {
                console.log(message);
            };
            return this;
        }();
        this.sendHandler = null;
        
        this.pushToUser = function (usr) {
            var m = { id: usr.connectionId, command: usr.messageToUser(), userId: usr.userId };
            if (that.sendHandler) {
                that.sendHandler.send(m);
                console.log(m);
            }
        };
        
        this.actOnMessageMouseOver = function(item) {
            setUserBackground(item, 'isLit');
        };
        
        this.actOnMessageMouseOut = function (item) {
            setUserBackground(item, 'isNotLit');
        };
        
        this.actOnUserMouseOver = function (item) {
            setMessageBackground(item, 'isLit');
        };

        this.actOnUserMouseOut = function (item) {
            setMessageBackground(item, 'isNotLit');
        };

        this.init = function (realtimeSrvr) {
            if (that.disconnetHandler) {
                that.disconnetHandler();
            }
            if (!realtimeSrvr || !that[realtimeSrvr + 'InitHandler']) {
                return;
            }
            that.serverInfo(realtimeSrvr);
            that.sendHandler = that[realtimeSrvr + 'InitHandler'](that);
        };

        this.signalRInitHandler = signalRInit;
        this.nodejsInitHandler = nodejsInit;
        this.iisnodeInitHandler = iisnodeInit;
        this.disconnetHandler = null;

        function signalRInit() {
            var url = 'http://localhost/RealTimeSignalRServer2';
            var connection = $.hubConnection(url);
            connection.logging = true;
            connection.qs = "myauthtoken=mySecretDashboard";
            var proxy = connection.createHubProxy('dashboardhub');
            proxy.on('message', function (data) {
                processReceivedData(data);

            });
            connection.start({ transport: 'webSockets' })
                .done(function() {
                    that.logger.log('Now connected to ' + url + ', connection ID=' + connection.id + ' UniqueId=');
                })
                .fail(function(err) { that.logger.log('Could not connect to ' + url + ': ' + err); });

            that.disconnetHandler = function() {
                that.logger.log('Disconnected from ' + url);
            };
            
            function send(data) {
                proxy.invoke('send', data).done(function () {
                    that.logger.log('Invocation of send to ' + url + ' succeeded');
                }).fail(function (error) {
                    that.logger.log('Invocation of send to ' + url + ' failed. Error: ' + error);
                });
            }
            
            return {
                send: send
            };
        }

        function nodejsInit() {
            var url = 'http://localhost:8887/dashboardhub?myauthtoken=mySecretDashboard';
            return nodejsCommon(url);
        }

        function iisnodeInit() {
            var url = 'http://localhost:9998/dashboardhub?myauthtoken=mySecretDashboard';
            return nodejsCommon(url);
        }

        function nodejsCommon(url) {
            var client = io.connect(url);

            client.on('message', function(data) {
                processReceivedData(data);
            });

            client.on('connect', function() {
                that.logger.log('Now connected to ' + url + ', connection ID=' + client.socket.sessionid + ' UniqueId=');
            });

            client.on('connect_failed', function (reason) {
                that.logger.log('Could not connect to ' + url + ": " + reason);
            });

            that.disconnetHandler = function() {
                that.logger.log('Disconnected from ' + url);
                client.disconnect();
            };
            
            function send(data) {
                client.emit('send', data);
                that.logger.log('Invocation of send to ' + url + ' succeeded');
            }

            return {
                send: send
            };
        }

        function processReceivedData(data) {
            if (data.type === 'clickCount') {
                var message = data.message.message;
                var msg = { type: data.type, id: data.message.id, message: formatDateTime(new Date(message.tm)) + ' - Click Count: ' + message.cnt };
                augmentMessage(msg);
                that.dataItems.unshift(msg);
                that.summary.lastUserActivityAt(formatDateTime(data.message.timestamp));
                that.logger.log('got clickCount data ');
            }

            if (data.type === 'greeting') {
                that.logger.log('unique id: ' + data.message);
            }
            
            if (data.type === 'userAdded') {
                that.logger.log('user added: ' + JSON.stringify(data.message));
                var user = data.message;
                augmentUser(user);
                that.users.unshift(user);
            }

            if (data.type === 'userRemoved') {
                that.logger.log('user removed: ' + JSON.stringify(data.message));
                var disconnectedUser = data.message;
                var match = _.find(that.users(), function(usr) {
                    return usr.connectionId === disconnectedUser.connectionId;
                });
                if (match) {
                    match.disconnectedAt(disconnectedUser.disconnectedAt);
                }
            }

            if (data.type === 'users') {
                that.logger.log('users: ' + JSON.stringify(data.message));
                $.each(data.message, function (index, item) {
                    var nextUser = item;
                    augmentUser(nextUser);
                    that.users.push(nextUser);
                });
            }
            
            if (data.type === 'summary') {
                that.logger.log('summary: ' + JSON.stringify(data.message));
                var summary = data.message;
                that.summary.activeUsers(summary.activeUsers);
                that.summary.inactiveUsers(summary.inactiveUsers);
                that.summary.firstUserConnectedAt(formatDateTime(summary.firstUserConnectedAt));
                that.summary.lastUserConnectedAt(formatDateTime(summary.lastUserConnectedAt));
                that.summary.lastUserActivityAt(formatDateTime(summary.lastUserActivityAt));
                that.summary.longestActivity(formatMiliseconds(summary.longestActivity));
                that.summary.shortestActivity(formatMiliseconds(summary.shortestActivity));
                that.summary.medianActivity(formatMiliseconds(summary.medianActivity));
                that.summary.averageActivity(formatMiliseconds(summary.averageActivity));
            }
            
            if (data.type === 'serviceCall') {
                var serviceMessage = data.message.message;
                var svcMsg = { type: data.type, id: data.message.id, message: formatDateTime(new Date(serviceMessage.tm)) + ' - ' + JSON.stringify(serviceMessage) };
                augmentMessage(svcMsg);
                that.dataItems.unshift(svcMsg);
                that.summary.lastUserActivityAt(formatDateTime(data.message.timestamp));
                that.logger.log('got serviceCall data ');
            }
            
            if (data.type === 'performance') {
                var performanceMessage = data.message.message;
                var perfMsg = { type: data.type, id: data.message.id, message: formatDateTime(new Date(performanceMessage.tm)) + ' - ' + JSON.stringify(performanceMessage) };
                augmentMessage(perfMsg);
                that.dataItems.unshift(perfMsg);
                that.summary.lastUserActivityAt(formatDateTime(data.message.timestamp));
                that.logger.log('got performance data ');
            }
            
            if (data.type === 'command') {
                var commandMessage = data.message.message;
                var cmdMsg = { type: data.type, id: data.message.id, message: formatDateTime(new Date(commandMessage.tm)) + ' - ' + JSON.stringify(commandMessage) };
                augmentMessage(cmdMsg);
                that.dataItems.unshift(cmdMsg);
                that.summary.lastUserActivityAt(formatDateTime(data.message.timestamp));
                that.logger.log('got command data ');
            }
            
            function augmentUser(hubUser) {
                hubUser.connectedAt = formatDateTime(hubUser.connectedAt);
                hubUser.disconnectedAt = ko.observable(formatDateTime(hubUser.disconnectedAt));
                hubUser.userStatusCss = ko.computed(function () {
                    return !hubUser.disconnectedAt() ? "userStatus statusActive" : "userStatus statusInactive";
                });
                hubUser.isLitCss = ko.observable();
                hubUser.canPush = ko.computed(function() {
                    return !hubUser.disconnectedAt();
                });
                hubUser.messageToUser = ko.observable();
            }
            
            function augmentMessage(msgItem) {
                msgItem.isLitCss = ko.observable();
            }
            
            function formatDateTime(dateTiemValue) {
                if (dateTiemValue) {
                    return moment(dateTiemValue).format('YYYY/MM/DD HH:mm:ss.SSS');
                }
                return '';
            }
            
            function formatMiliseconds(number) {
                //return number > 0 ? (0.001 * number).toFixed(0).toLocaleString() : '';
                return (moment.duration(number).asMinutes()).toFixed(0);
            }
        }
        
        function findUser(usrId) {
            return _.find(that.users(), function (usr) { return usr.connectionId === usrId; });
        }
        
        function setUserBackground(item, cssClass) {
            if (!item || !item.id) {
                return;
            }
            var user = findUser(item.id);
            if (user) {
                user.isLitCss(cssClass);
            }
        }
        
        function findMessages(usrId) {
            return _.filter(that.dataItems(), function (itm) { return itm.id === usrId; });
        }
        
        function setMessageBackground(usr, cssClass) {
            if (!usr || !usr.connectionId) {
                return;
            }
            var msgs = findMessages(usr.connectionId);
            if (msgs) {
                
                _.each(msgs, function(itm) {
                    itm.isLitCss(cssClass);
                });
            }
        }

        return this;

    })();

    return dashboardViewModel;
});
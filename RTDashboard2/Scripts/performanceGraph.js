﻿define(function () {

    var PerformanceGraph = function (containerName, initialData, colorMapper) {
        
        var w = 20, h = 210;

        var x = d3.scale.linear()
            .domain([0, 1])
            .range([0, w]);

        var y = d3.scale.linear()
            .domain([0, 200])
            .rangeRound([0, h]);
        
        var chart3 = d3.select(containerName).append("svg")
            .attr("class", "chart")
            .attr("width", w * initialData.length - 1)
            .attr("height", h);

        chart3.append("line")
            .attr("x1", 0)
            .attr("x2", w * initialData.length)
            .attr("y1", h - .5)
            .attr("y2", h - .5)
            .style("stroke", "#000");

        redraw3(initialData);

        function redraw3(data) {

            var rect = chart3.selectAll("rect")
                .data(data, function(d) { return d.time; });

            rect.enter().insert("rect", "line")
                .attr("x", function(d, i) { return x(i + 1) - .5; })
                .attr("y", function(d) { return h - y(d.value) - .5; })
                .attr("width", w)
                .attr("height", function (d) { return y(d.value); })
                .attr('fill', function(d) {
                    var color = colorMapper[d.id];
                    return color ? color : 'rgb(169,169,169)'; //dark gray
                })
    //            .append("text")
    //.attr("dy", ".75em")
    //.attr("y", function (d) { return h - y(d.value) - .5; })
    //.attr("x", function(d, i) { return x(i + 1) - .5; })
    //.attr("text-anchor", "middle")
    //.attr('fill', 'white')
    //.text(function(d) { return d.value; })
                .transition()
                .duration(1000)
                .attr("x", function(d, i) { return x(i) - .5; });

            rect.transition()
                .duration(1000)
                .attr("x", function(d, i) { return x(i) - .5; });

            rect.exit().transition()
                .duration(1000)
                .attr("x", function(d, i) { return x(i - 1) - .5; })
                .remove();
            
            var txt = chart3.selectAll("text")
                .data(data, function (d) { return d.time; });

            txt.enter().insert("text")
                .attr("dy", ".5em")
                .attr("x", function(d, i) { return x(i); })
                .attr("y", function(d) { return h - y(d.value) + 4; })
                .text(function(d) { return d.value > 10 ? d.value : ''; })
                .attr("text-anchor", "right")
                .attr('fill', 'white')
                .transition()
                .duration(1000)
                .attr("x", function (d, i) { return x(i); });
            
            txt.transition()
              .duration(1000)
              .attr("x", function (d, i) { return x(i) - .5; });

            txt.exit().transition()
                .duration(1000)
                .attr("x", function (d, i) { return x(i - 1) - .5; })
                .remove();
        }

        return { refresh: redraw3 };
    };

    return PerformanceGraph;
});
﻿define([], function () {

    var clicks = 0;
    
    function getRandomInt(from, to) {
        return Math.floor((Math.random() * (to - from + 1)) + from);
    }

    function getRandomDecimal(numOfDigits) {
        return +Math.random().toFixed(numOfDigits);
    }

    function getRandomParam(paramid, from, to) {
        return '"param' + paramid + '":  "' + getRandomInt(from, to) + '"';
    }

    function getBoundingBox() {
        //+48.987386 northern most latitude 
        //+18.005611 southern most latitude 
        //-124.626080 west most longitude 
        //-62.361014 east most longitude 
        var tl, br,
            t = getRandomInt(27, 47) + getRandomDecimal(5),
            l = -1 * (getRandomInt(72, 124) + getRandomDecimal(5)),
            b = getRandomInt(27, 47) + getRandomDecimal(5),
            r = -1 * (getRandomInt(72, 124) + getRandomDecimal(5));
        tl = [Math.max(t, b), Math.min(l, r)];
        br = [Math.min(t, b), Math.max(l, r)];

        return { tl: tl, br: br };
    }
    
    function buildClickMessage() {
        clicks++;
        return { type: 'clickCount', message: { tm: (new Date()).valueOf(), cnt: clicks } };
    }
    
    function buildServiceCallMessage() {
        var serviceUrl, jsonString = '', requestJson, elapsedTime, resultsCount, i;
        serviceUrl = 'http://localhost/service' + getRandomInt(1, 5);
        for (i = 0; i < 10; i++) {
            if (i <= getRandomInt(0, 10)) {
                jsonString += getRandomParam(i + 1, 0, (i + 1) * 100) + ",";
            }
        }

        if (jsonString.length > 0) {
            jsonString = jsonString.substring(0, jsonString.length - 1);
        }
        jsonString = "{" + jsonString + "}";

        requestJson = JSON.parse(jsonString);

        requestJson.bb = getBoundingBox();

        elapsedTime = getRandomInt(400, 2000);
        resultsCount = getRandomInt(1, 5000);

        return { type: 'serviceCall', message: { tm: (new Date()).valueOf(), su: serviceUrl, rj: requestJson, et: elapsedTime, rc: resultsCount } };
    }
    
    function buildPerformanceMessage() {
        var performanceMessageId, elapsedTime, performanceMessages = ['init', 'func', 'func2', 'func3', 'func12', 'func19'];
        performanceMessageId = performanceMessages[getRandomInt(0, 5)];
        elapsedTime = getRandomInt(1, 200);

        return { type: 'performance', message: { id: performanceMessageId, tm: (new Date()).valueOf(), et: elapsedTime } };
    }

    return {
        buildPerformanceMessage: buildPerformanceMessage,
        buildServiceCallMessage: buildServiceCallMessage,
        buildClickMessage: buildClickMessage
    };
});
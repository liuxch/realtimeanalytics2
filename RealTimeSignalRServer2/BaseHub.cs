﻿using System.Threading.Tasks;
using Microsoft.AspNet.SignalR;
using System.Collections.Concurrent;

namespace RealTimeSignalRServer2
{
    using System;
    using System.Collections.Generic;
    using System.Security;
    using System.Web.Http.Validation.Validators;

    using Microsoft.AspNet.SignalR.Hubs;

    public class BaseHub : Hub
    {
        private readonly ConcurrentDictionary<string, UserMessagePayload> _hubUsers = new ConcurrentDictionary<string, UserMessagePayload>();

        public override Task OnConnected()
        {
            // Add your own code here.
            // For example: in a chat application, record the association between
            // the current connection ID and user name, and mark the user as online.
            // After the code in this method completes, the client is informed that
            // the connection is established; for example, in a JavaScript client,
            // the start().done callback is executed.
            //var userName = Clients.Caller.state.userName;
            //var user = this.CreateUser();
            this.SendGreeting();
            return base.OnConnected();
        }


        public override Task OnDisconnected()
        {
            // Add your own code here.
            // For example: in a chat application, mark the user as offline, 
            // delete the association between the current connection id and user name.
            return base.OnDisconnected();
        }

        //public override Task OnReconnected()
        //{
        //    // Add your own code here.
        //    // For example: in a chat application, you might have marked the
        //    // user as offline after a period of inactivity; in that case 
        //    // mark the user as online again.
        //    return base.OnReconnected();
        //}

        public void SendGreeting()
        {
            this._hubUsers.TryAdd(Context.ConnectionId, this.CreateUser());

            Clients.Caller.Message(new HubMessage { Type = "greeting", Message = this._hubUsers[Context.ConnectionId].UserId });
        }

        protected string CreateUserId()
        {
            return "signalR_" + Guid.NewGuid().ToString();
        }

        protected UserMessagePayload CreateUser()
        {
            var user = new UserMessagePayload
            {
                ConnectedAt = DateTime.UtcNow,
                // ConnectedFrom = Context.Request.Headers
                UserId = this.CreateUserId(),
                ConnectionId = Context.ConnectionId
            };

            return user;
        }

        protected bool AuthenticateUser(string validSecret)
        {
            return this.Context.Request.QueryString["myauthtoken"] == null || this.Context.Request.QueryString["myauthtoken"] == validSecret;
        }
    }
}
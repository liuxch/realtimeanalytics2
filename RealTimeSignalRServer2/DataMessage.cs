﻿using Newtonsoft.Json;

namespace RealTimeSignalRServer2
{

    public class DataMessage
    {
        [JsonProperty("type")] 
        public string Type { get; set; }

        [JsonProperty("message")] 
        public DataMessagePayload Message { get; set; }
    }
}
﻿using Newtonsoft.Json;
using System;

namespace RealTimeSignalRServer2
{
    public class DataMessagePayload
    {
        [JsonProperty("id")] 
        public string Id { get; set; }

        [JsonProperty("message")] 
        public string Message { get; set; }

        [JsonProperty("timestamp")]
        public DateTime Timestamp { get; set; }
    }
}
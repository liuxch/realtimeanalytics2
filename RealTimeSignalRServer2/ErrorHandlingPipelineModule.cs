﻿using Microsoft.AspNet.SignalR.Hubs;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Web;

namespace RealTimeSignalRServer2
{
    public class ErrorHandlingPipelineModule : HubPipelineModule
    {
        protected override void OnIncomingError(ExceptionContext exceptionContext, IHubIncomingInvokerContext invokerContext)
        {
            base.OnIncomingError(exceptionContext, invokerContext);
        }
        //protected override void OnIncomingError(Exception ex, IHubIncomingInvokerContext context)
        //{
        //    Debug.WriteLine("=> Exception " + ex.Message);
        //    if (ex.InnerException != null)
        //    {
        //        Debug.WriteLine("=> Inner Exception " + ex.InnerException.Message);
        //    }
        //    base.OnIncomingError(ex, context);
        //}

    }
}
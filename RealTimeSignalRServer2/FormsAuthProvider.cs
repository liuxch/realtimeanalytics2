﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RealTimeSignalRServer2
{
    using System.Web.Security;

    public class FormsAuthProvider : IAuthProvider
    {
        public bool Authenticate(string username, string password)
        {
            var result = username == "x" && password == "y"; 
            // FormsAuthentication.Authenticate(username, password);

            if (result)
            {
                FormsAuthentication.SetAuthCookie(username, false);
            }

            return result;
        }
    }
}
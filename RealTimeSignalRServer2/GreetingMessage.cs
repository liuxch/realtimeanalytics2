﻿using Newtonsoft.Json;

namespace RealTimeSignalRServer2
{
    public class GreetingMessage
    {
        [JsonProperty("type")] 
        public string Type { get; set; }

        [JsonProperty("message")] 
        public string Message { get; set; }
    }
}
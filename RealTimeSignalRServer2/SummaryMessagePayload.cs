﻿namespace RealTimeSignalRServer2
{
    using System;

    using Newtonsoft.Json;

    public class SummaryMessagePayload
    {
        [JsonProperty("activeUsers")]
        public int ActiveUsers { get; set; }

        [JsonProperty("inactiveUsers")]
        public int InactiveUsers { get; set; }

        [JsonProperty("firstUserConnectedAt")]
        public DateTime? FirstUserConnectedAt { get; set; }

        [JsonProperty("lastUserConnectedAt")]
        public DateTime? LastUserConnectedAt { get; set; }

        [JsonProperty("lastUserActivityAt")]
        public DateTime? LastUserActivityAt { get; set; }

        [JsonProperty("longestActivity")]
        public int LongestActivityMinutes { get; set; }

        [JsonProperty("shortestActivity")]
        public int ShortestActivityMinutes { get; set; }

        [JsonProperty("medianActivity")]
        public int MedianActivityMinutes { get; set; }

        [JsonProperty("averageActivity")]
        public int AverageActivityMinutes { get; set; }
    }
}
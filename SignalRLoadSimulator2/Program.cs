﻿using System;
using System.Dynamic;
using System.Threading;
using System.Threading.Tasks;

using Microsoft.AspNet.SignalR.Client;
using Microsoft.AspNet.SignalR.Client.Hubs;
using Microsoft.AspNet.SignalR.Client.Transports;

namespace SignalRLoadSimulator2
{
    internal class Program
    {
        private static string url = "http://localhost/RealTimeSignalRServer2/";

        private static int createTimeDelay = 500;

        private static Random randomInterval = new Random();

        private static MessageBuilder messageBuider = new MessageBuilder();

        private static void Main(string[] args)
        {
            //ServicePointManager.DefaultConnectionLimit = 10;
            for (var i = 0; i < 10; i++)
            {
                var delay = (i + 1) * createTimeDelay;
                var task = WaitForNewClient(delay, randomInterval.Next(10000, 30000));
               task.Wait();
            }

            Thread.Sleep(1000 * 1000);
        }

        private static async Task WaitForNewClient(int delay, int sendInterval)
        {
            await CreateClient(delay, sendInterval);
        }
    
        private static async Task<HubConnection> CreateClient(int delay, int sendInterval)
        {

            var disconnectTime = DateTime.Now.AddMilliseconds(sendInterval * randomInterval.Next(1, 100));

            var sendTimer = new System.Timers.Timer();
            sendTimer.Interval = sendInterval;
            sendTimer.Enabled = true;

            var hubConnection = new HubConnection(url, "myauthtoken=mySecret");
            hubConnection.TraceLevel = TraceLevels.All;
            hubConnection.TraceWriter = Console.Out;
            IHubProxy proxy = hubConnection.CreateHubProxy("analyticshub");

            proxy.On("Message", message => ProcessReceivedData(message));

            hubConnection.Closed += () => Console.WriteLine("**************************** Connection closed.");
            hubConnection.Error += (e) => Console.WriteLine("Connection error: " + e.Message);

            await Task.Delay(delay);
           
            hubConnection.Start(new WebSocketTransport()).Wait();

            Console.WriteLine("Now connected to " + url + ", connection ID=" + hubConnection.ConnectionId + " UniqueId=");
            Console.WriteLine("send interval: " + sendInterval + ", disconnect after: " + disconnectTime);
            Console.WriteLine("current time: " + DateTime.Now);

            SetupTimer(sendTimer, hubConnection, proxy, messageBuider, disconnectTime, 1);
            SetupTimer(sendTimer, hubConnection, proxy, messageBuider, disconnectTime, 2);
            SetupTimer(sendTimer, hubConnection, proxy, messageBuider, disconnectTime, 3);

            return hubConnection;
        }

        static void SetupTimer(System.Timers.Timer sendTimer, HubConnection hubConnection, IHubProxy proxy, MessageBuilder builder, DateTime disconnectTime, int messageType)
        {
            sendTimer.Elapsed += (sender, args) => proxy.Invoke<HubMessage>("Send", messageType == 1
                ? builder.BuildClickMessage()
                : messageType == 2 ? builder.BuildServiceCallMessage() : builder.BuildPerformanceMessage()).ContinueWith(
               task =>
               {
                   Console.WriteLine(
                       "Invocation of send from " + hubConnection.ConnectionId + " to " + url + " succeeded at "
                       + DateTime.UtcNow);
                   if (DateTime.Now > disconnectTime)
                   {
                       sendTimer.Stop();
                       hubConnection.Stop();
                   }
               });
        }

        static void ProcessReceivedData(dynamic message)
        {
            if (message.type == "greeting")
            {
                Console.WriteLine("unique id: {0}", message.message);
            }
        }

        private class HubMessage
        {
            public string type { get; set; }

            public dynamic message { get; set; }
        }

        private class MessageBuilder
        {
            private int _clicks = 0;
            private readonly Random _rnd = new Random();
    
            private int GetRandomInt(int from, int to) {
                return this._rnd.Next(from, to);
            }

            private double GetRandomDouble(int numOfDigits) {
                return Math.Round(this._rnd.NextDouble(), numOfDigits);
            }

            private dynamic GetBoundingBox() {
                //+48.987386 northern most latitude 
                //+18.005611 southern most latitude 
                //-124.626080 west most longitude 
                //-62.361014 east most longitude 
                //var tl;
                //var br;
                var t = GetRandomInt(27, 47) + GetRandomDouble(5);
                var l = -1 * (GetRandomInt(72, 124) + GetRandomDouble(5));
                var b = GetRandomInt(27, 47) + GetRandomDouble(5);
                var r = -1 * (GetRandomInt(72, 124) + GetRandomDouble(5));
                var tl = new double[] { Math.Max(t, b), Math.Min(l, r) };
                var br = new double[] { Math.Min(t, b), Math.Max(l, r) };

                return new { tl, br };
            }

            public HubMessage BuildClickMessage()
            {
                this._clicks++;
                return new HubMessage() { type = "clickCount", message = new { tm = DateTime.UtcNow, cnt = this._clicks } };
            }

            public HubMessage BuildServiceCallMessage()
            {
                var serviceUrl = "http://localhost/service" + GetRandomInt(1, 5);
                dynamic requestJson = new ExpandoObject();

                for (var i = 0; i < 10; i++) {
                    if (i <= GetRandomInt(0, 10))
                    {
                        var from = 0;
                        var to = (i + 1) * 100;
                        switch (i + 1)
                        {
                            case 1:
                                requestJson.param1 = GetRandomInt(from, to);
                                break;
                            case 2:
                                requestJson.param2 = GetRandomInt(from, to);
                                break;
                            case 3:
                                requestJson.param3 = GetRandomInt(from, to);
                                break;
                            case 4:
                                requestJson.param4 = GetRandomInt(from, to);
                                break;
                            case 5:
                                requestJson.param5 = GetRandomInt(from, to);
                                break;
                            case 6:
                                requestJson.param6 = GetRandomInt(from, to);
                                break;
                            case 7:
                                requestJson.param7 = GetRandomInt(from, to);
                                break;
                            case 8:
                                requestJson.param8 = GetRandomInt(from, to);
                                break;
                            case 9:
                                requestJson.param9 = GetRandomInt(from, to);
                                break;
                            case 10:
                                requestJson.param10 = GetRandomInt(from, to);
                                break;
                        }

                    }
                }

                requestJson.bb = GetBoundingBox();

                var elapsedTime = this.GetRandomInt(400, 2000);
                var resultsCount = this.GetRandomInt(1, 5000);

                return new HubMessage() { type = "serviceCall", message = new { tm = DateTime.UtcNow, su = serviceUrl, rj = requestJson, et = elapsedTime, rc = resultsCount } };
            }

            public HubMessage BuildPerformanceMessage()
            {
                var elapsedTime = GetRandomInt(1, 200);
                var performanceMessages = new [] { "init", "func", "func2", "func3", "func12", "func19" };
                var performanceMessageId = performanceMessages[GetRandomInt(0, 5)];

                return new HubMessage() { type = "performance", message = new { id = performanceMessageId, tm = DateTime.UtcNow, et = elapsedTime } };
            }
        }
    }
}
